package net.onciu.fragmentsfun;

import android.app.Application;
import com.parse.ParseObject;
import net.onciu.fragmentsfun.models.UserPhoto;

/**
 * Created with IntelliJ IDEA.
 * User: Cristian Onciu
 * Date: 2/14/14
 * Time: 5:38 PM
 */
public class SwipeApplication extends Application {

    public SwipeApplication(){
        ParseObject.registerSubclass(UserPhoto.class);
    }
}
