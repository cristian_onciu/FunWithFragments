package net.onciu.fragmentsfun.models;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created with IntelliJ IDEA.
 * User: Cristian Onciu
 * Date: 2/14/14
 * Time: 5:05 PM
 */
@ParseClassName("UserPhoto")
public class UserPhoto extends ParseObject {

    public UserPhoto() {
    }

    public String getUserComment() {
        return getString("userComment");
    }

    public void setUserComment(String userComment) {
        put("userComment", userComment);
    }

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser user) {
        put("user", user);
    }

    public ParseFile getImageFile() {
        return getParseFile("imageFile");
    }

    public void setImageFile(ParseFile file) {
        put("imageFile", file);
    }

    public ParseFile getThumbnail() {
        return getParseFile("thumbnail");
    }

    public void setThumbnail(ParseFile file) {
        put("thumbnail", file);
    }
}
