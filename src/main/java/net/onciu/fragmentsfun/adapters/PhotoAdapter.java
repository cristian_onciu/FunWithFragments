package net.onciu.fragmentsfun.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.parse.*;
import net.onciu.fragmentsfun.MainActivity;
import net.onciu.fragmentsfun.R;
import net.onciu.fragmentsfun.models.UserPhoto;

/**
 * Created with IntelliJ IDEA.
 * User: Cristian Onciu
 * Date: 2/14/14
 * Time: 5:00 PM
 */
public class PhotoAdapter extends ParseQueryAdapter<UserPhoto> {
    public PhotoAdapter(Context context) {
        super(context, new QueryFactory<UserPhoto>() {
            @Override
            public ParseQuery<UserPhoto> create() {
                ParseQuery query = new ParseQuery("UserPhoto");
                query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
                return query;
            }
        });
    }

    @Override
    public View getItemView(UserPhoto userPhoto, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.list_item, null);
        }

        super.getItemView(userPhoto, v, parent);

        ParseImageView userImage = (ParseImageView) v.findViewById(R.id.icon);
        //ParseFile imageView = userPhoto.getParseFile("imageFile");
        ParseFile imageView = userPhoto.getParseFile("thumbnail");
        if (imageView != null) {
            userImage.setParseFile(imageView);
            userImage.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    // nothing to do
                }
            });
        }

        TextView textViewRowContent= (TextView) v.findViewById(R.id.textViewRowContent);
        textViewRowContent.setText(userPhoto.getUserComment());
        v.setOnTouchListener(MainActivity.gestureListener);
        return v;
    }
}
