package net.onciu.fragmentsfun;

import android.app.Activity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
* Created with IntelliJ IDEA.
* User: Cristian Onciu
* Date: 2/13/14
* Time: 8:57 PM
*/
final class GestureListener extends GestureDetector.SimpleOnGestureListener {

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;
    public static final int LEFT=1;
    public static final int RIGHT=2;
    private OnGestureCallback onGestureCallback;

    public interface OnGestureCallback{
        public void gestureDone(int gesture);
    }

    public GestureListener(Activity activity){
        try {
            onGestureCallback= (OnGestureCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnGestureCallback");
        }

    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        boolean result = false;
        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        Log.d("LOG", "SWIPE");
                        onSwipeRight();
                    } else {
                        Log.d("LOG","SWIPE");
                        onSwipeLeft();
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        Log.d("LOG","SWIPE");
                        onSwipeBottom();
                    } else {
                        Log.d("LOG","SWIPE");
                        onSwipeTop();
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    public void onSwipeRight() {
        onGestureCallback.gestureDone(RIGHT);
    }

    public void onSwipeLeft() {
        onGestureCallback.gestureDone(LEFT);
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }

}
